package saf.components;

/**
 * This interface serves as a family of type that will initialize
 * the style for some set of controls, like the workspace, for example.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public interface AppStyleArbiter {
    // THESE ARE COMMON STYLE CLASSES WE'LL USE
    public static final String CLASS_BORDERED_PANE = "bordered_pane";
    public static final String CLASS_FILE_PANE = "file_pane";
    public static final String CLASS_HEADING_LABEL = "heading_label";
    public static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    public static final String CLASS_PROMPT_LABEL = "prompt_label";
    public static final String CLASS_PROMPT_TEXT_FIELD = "prompt_text_field";
    public static final String CLASS_TOOLBAR_BUTTON = "toolbar_button";
    public static final String CLASS_VERT_OPTIONS = "vert_options";
    public static final String CLASS_EXPORT_BUTTONS = "export_button";
    public static final String CLASS_VIEW_TOGGLES = "view_toggle";
    public static final String CLASS_EDIT_PANE = "edit_pane";
    public static final String CLASS_VIEW_PANE = "view_pane";
    public static final String CLASS_TEXT = "side_text";
    public static final String CLASS_SIDE_PANE = "side_pane";
    public static final String CLASS_SIDEBAR_BUTTON = "sidebar_button";
    public static final String CLASS_TABLE = "table_view";
    
    public void initStyle();
}
